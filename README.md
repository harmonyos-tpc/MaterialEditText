## Features
1. **Basic**

  ![Basic](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/basic.png)
  
2. **Floating Label**
  
  normal:
  
  ![FloatingLabel](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/floating_label.png)
  
  highlight:
  
  ![HighlightFloatingLabel](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/highlight.png)

  custom floating label text:

  ![CustomFloatingLabelText](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/custom_floating_label_text.png)
  
3. **Single Line Ellipsis**
  
  ![SingLineEllipsis](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/ellipsis.png)
  
4. **Max/Min Characters**
  
  ![MaxCharacters](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/max_characters.png)

  ![MinCharacters](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/min_characters.png)

  ![MinAndMaxCharacters](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/min_and_max.png)
  
5. **Helper Text and Error Text**

  ![HelperTextAndErrorText](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/helper_error_text.png)

6. **Custom Base/Primary/Error/HelperText Colors**

  ![CustomColors](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/custom_colors.png)

7. **Custom accent typeface**

  floating label, error/helper text, character counter, etc.

  ![CustomAccentTypeface](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/custom_accent_typeface.png)

8. **Hide Underline**

  ![HideUnderLine](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/hide_underline.png)

8. **Material Design Icon**

  ![MaterialDesignIcon](https://gitee.com/openharmony-tpc/MaterialEditText/raw/master/image/material_design_icon.png)

# **How to use?**

```
dependencies{
    implementation 'io.openharmony.tpc.thirdlib:MaterialEditText:1.0.1'
}
```

``` xml
<com.rengwuxian.materialedittext.MaterialEditText
        ohos:height="match_content"
        ohos:width="match_parent"
        app:met_textSize="15vp"
        app:met_textColor="#FFA3ED"
        app:met_iconLeft="$media:ic_phone"
        app:met_iconRight="$media:ic_action_accept"
        app:met_text="gogogo"
        app:met_hint="hint"
        app:met_underlineColor = "#FFA3ED"
        app:met_clearButton="true"
        app:met_baseColor="black"
        app:met_floatingLabel="highlight"
        app:met_floatingLabelTextColor="#74FFB1"
        app:met_maxCharacters="40"
        app:met_primaryColor="#7d7d7d"
        app:met_singleLineEllipsis="true"
        app:met_floatingLabelAlwaysShown="true"
        app:met_helperText="helper is here"
        app:met_cursorColor="#ffff00"
        />
```

# API:
## MaterialEditText

**public void setIconLeft(int res)**
- description: set left icon by res id

**public void setIconLeft(Element drawable)**
- description: set left icon by Element

**public void setIconLeft(PixelMap pixelMap)**
- description: set left icon by PixelMap

**public void setIconRight(int res)**
- description: set right icon by res id

**public void setIconRight(Element element)**
- description: set right icon by Element

**public void setIconRight(PixelMap pixelMap)**
- description: set right icon by resId

**public boolean isShowClearButton()**
- description: get button clear show

**public void setShowClearButton(boolean show)**
- description: set button clear show

**public boolean isFloatingLabelAlwaysShown()**
- description: get floating label always show

**public void setFloatingLabelAlwaysShown(boolean floatingLabelAlwaysShown)**
- description: set floating label always show

**public boolean isHelperTextAlwaysShown()**
- description: get helper text always show

**public void setHelperTextAlwaysShown(boolean helperTextAlwaysShown)**
- description: set helper text always show

**public Font getAccentTypeface()**
- description: get accent Font

**public void setAccentTypeface(Font accentTypeface)**
- description: set accent Font

**public boolean isHideUnderline()**
- description: get underline hide

**public void setHideUnderline(boolean hideUnderline)**
- description: set underline hide

**public int getUnderlineColor()**
- description: get underline color

**public void setUnderlineColor(int color)**
- description: set underline color

**public String getFloatingLabelText()**
- description: get floating label text

**public void setFloatingLabelText(String floatingLabelText)**
- description: set floating label text'

**public int getFloatingLabelTextSize()**
- description: get floating label text size

**public void setFloatingLabelTextSize(int size)**
- description: set floating label text size

**public int getFloatingLabelTextColor()**
- description: get floating label text color

**public void setFloatingLabelTextColor(int color)**
- description: set floating label text color

**public int getBottomTextSize()**
- description: get bottom text size

**public void setBottomTextSize(int size)**
- description: set bottom text size

**public void setBaseColor(int color)**
- description: set base color

**public void setPrimaryColor(int color)**
- description: set primary color

**public void setMetTextColor(ColorStateList colors)**
- description: set text color by ColorStateList

**public void setMetHintTextColor(ColorStateList colors)**
- description: set hint text color by ColorStateList

**public void setFloatingLabel(int mode)**
- description: set floating label mode

**public void setSingleLineEllipsis()**
- description: set single line

**public void setSingleLineEllipsis(boolean enabled)**
- description: set single line

**public int getMaxCharacters()**
- description: get max characters

**public void setMaxCharacters(int max)**
- description: set max characters

**public int getMinCharacters()**
- description: get min characters

**public void setMinCharacters(int min)**
- description: set min characters

**public String getError()**
- description: get error text

**public void setError(String error)**
- description: set error text

**public String getHelperText()**
- description: get helper text

**public void setHelperText(String helperText)**
- description: set helper text

**public int getErrorColor()**
- description: get error text color

**public void setErrorColor(int color)**
- description: set error text color

**public int getHelperTextColor()**
- description: get helper text color

**public void setHelperTextColor(int color)**
- description: set helper text color

**public void setEnabled(boolean enable)**
- description: set textfield enable 

**public boolean isEnabled()**
- description: get textfield enable 

**public void setTextSize(int size)**
- description: set textfield textSize 

# AttrSet
|name|format|description|
|:---:|:---:|:---:|
| met_enabled | boolean | set textfield enable
| met_textSize | dimension | set textfield text size
| met_text | string | set textfield text
| met_hint | string | set textfield hint text
| met_textColor | color | set textfield text default color
| met_textColor_disabled | color | set textfield text disabled color
| met_textColorHint | color | set textfield text hint default color
| met_textColorHint_disabled | color | set textfield text hint disabled color
| met_baseColor | color | set base color
| met_textColorHint_disabled | color | set textfield text hint disabled color
| met_typeface | string | set textfield font
| met_accentTypeface | color | set accent font
| met_primaryColor | color | set primary color
| met_cursorColor | color | set cursor color
| met_floatingLabel | string | set floating label mode
| met_errorColor | color | set error text color
| met_minCharacters | color | set min characters
| met_maxCharacters | color | set max characters
| met_singleLineEllipsis | boolean | set textfield single line
| met_helperText | string | set helper text
| met_helperTextColor | color | set helper text color
| met_bottomSpacing | dimension | set bottom spacing
| met_floatingLabelText | string | set floating label text
| met_floatingLabelPadding | dimension | set floating label padding
| met_floatingLabelTextSize | dimension | set floating label text size
| met_floatingLabelTextColor | color | set floating label text color
| met_bottomTextSize | dimension | set bottom text size
| met_hideUnderline | boolean | set underline hide
| met_underlineColor | color | set underline color
| met_iconLeftWidth | dimension | set left icon width
| met_iconLeftHeight | dimension | set left icon height
| met_iconRightWidth | dimension | set right icon width
| met_iconRightHeight | dimension | set right icon height
| met_iconLeftMarginText | dimension | set left icon margin text
| met_iconRightMarginText | dimension | set right icon margin text
| met_iconLeftMarginBottom | dimension | set left icon margin bottom
| met_iconRightMarginBottom | dimension | set right icon margin bottom
| met_clearButton | boolean | set clear button shown
| met_floatingLabelAlwaysShown | boolean | set floating label always shown
| met_checkCharactersCountAtBeginning | boolean | check characters count at beginning


## License

    Copyright 2014 rengwuxian

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.






