package com.rengwuxian.materialedittext;

/**
 * Created by Administrator on 2014/12/12.
 */
public class Colors {
    public static boolean isLight(int color) {
        return Math.sqrt(
                        red(color) * red(color) * .241
                                + green(color) * green(color) * .691
                                + blue(color) * blue(color) * .068)
                > 130;
    }

    public static int red(int color) {
        return (color >> 16) & 0xFF;
    }

    public static int green(int color) {
        return (color >> 8) & 0xFF;
    }

    public static int blue(int color) {
        return color & 0xFF;
    }
}
