package com.rengwuxian.materialedittext;

import ohos.app.Context;

/**
 * Created by Zhukai on 2014/5/29 0029.
 */
public class Density {
    public static int dp2px(Context context, float dp) {
        return (int) (context.getResourceManager().getDeviceCapability().screenDensity / 160 * dp);
    }
}
