package com.rengwuxian.materialedittext;

import java.util.HashMap;

public class ColorStateList {
    HashMap<Integer, Integer> colorStateList = new HashMap<>();
    int defaultColor = 0;

    public void addState(int[] states, int color) {
        if (states != null && states.length > 0) {
            for (int i = 0; i < states.length; i++) {
                colorStateList.put(states[i], color);
            }
        }
        if (states != null && states.length == 0) {
            defaultColor = 0;
        }
    }

    public int getColorForState(int[] states, int color) {
        if (states != null && states.length > 0) {
            for (int i = 0; i < states.length; i++) {
                if (colorStateList.get(states[i]) != null) {
                    return colorStateList.get(states[i]);
                }
            }
        }
        if (states != null && states.length == 0) {
            return defaultColor;
        }
        return color;
    }
}
