package com.rengwuxian.materialedittext.entry;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rengwuxian.materialedittext.validation.RegexpValidator;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class MainAbility extends Ability {
    MaterialEditText basicEt;
    Button enableBt;
    Button setErrorBt;
    Button setError2Bt;
    Button setError3Bt;
    MaterialEditText bottomTextEt;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_sample);

        basicEt = (MaterialEditText) findComponentById(ResourceTable.Id_basicEt);
        enableBt = (Button) findComponentById(ResourceTable.Id_enableBt);

        enableBt.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        basicEt.setEnabled(!basicEt.isEnabled());
                        enableBt.setText(basicEt.isEnabled() ? "DISABLE" : "ENABLE");
                    }
                });

        bottomTextEt = (MaterialEditText) findComponentById(ResourceTable.Id_bottomTextEt);
        setErrorBt = (Button) findComponentById(ResourceTable.Id_setErrorBt);
        setError2Bt = (Button) findComponentById(ResourceTable.Id_setError2Bt);
        setError3Bt = (Button) findComponentById(ResourceTable.Id_setError3Bt);
        setErrorBt.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        bottomTextEt.setError("1-line Error!");
                    }
                });

        setError2Bt.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        bottomTextEt.setError("2-line\nError!");
                    }
                });

        setError3Bt.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        bottomTextEt.setError(
                                "So Many Errors! So Many Errors! So Many Errors! So Many Errors! So Many Errors! So"
                                    + " Many Errors! So Many Errors! So Many Errors!");
                    }
                });

        initValidationEt();
    }

    private void initValidationEt() {
        MaterialEditText validationEt = (MaterialEditText) findComponentById(ResourceTable.Id_validationEt);
        validationEt.addValidator(new RegexpValidator("Only Integer Valid!", "\\d+"));
        final Button validateBt = (Button) findComponentById(ResourceTable.Id_validateBt);
        validateBt.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        validationEt.validate();
                    }
                });
    }
}
